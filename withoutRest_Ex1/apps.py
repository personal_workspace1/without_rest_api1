from django.apps import AppConfig


class WithoutrestEx1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'withoutRest_Ex1'
